<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Taehyung;

class TaehyungController extends Controller
{
    public function index(){
        $data = Taehyung::all();
        return view('ibuhamil',compact('data'));
    }

    public function tambahdata(){
        return view('tambahdata');
    }
    
    public function insertdata(Request $request){
        dd($request->all());
       Taehyung::create($request->all());
       return redirect()->route('ibuhamil')->with('success','data berhasil ditambahkan');
    
    }
    public function tampilkandata($id){
      
        $data =Taehyung::find($id);
       // dd($data);

       return view('tampil',compact('data'));
    }

    public function updatedata(Request $request, $id){
        $data =Taehyung::find($id);
        $data->update($request->all());
        return redirect()->route('ibuhamil')->with('success','data berhasil diupdate');

    }
}
