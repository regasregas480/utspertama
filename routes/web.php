<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaehyungController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/data',[TaehyungController::class, 'index'])->name('data');

Route::get('/ibuhamil',[TaehyungController::class, 'ibuhamil'])->name('ibuhamil');

Route::get('/tambahdata',[TaehyungController::class, 'tambahdata'])->name('tambahdata');
Route::post('/insertdata',[TaehyungController::class, 'insertdata'])->name('insertdata');

Route::get('/tampilkandata/{id}',[TaehyungController::class, 'tampilkandata'])->name('tampilkandata');
Route::post('/updatedata/{id}',[TaehyungController::class, 'updatedata'])->name('updataedata');

