<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaehyungsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taehyungs', function (Blueprint $table) {
            $table->id();
            $table->string('namaibuk');
            $table->string('namaayah');
            $table->integer('beratbadan');
            $table->bigInteger('usiakandungan');
            $table->bigInteger('notelpon');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taehyungs');
    }
}
