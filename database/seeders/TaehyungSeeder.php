<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaehyungSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("taehyungs")->insert([
            'namaibuk' => 'maimuna',
            'namaayah' => 'suheri',
            'beratbadan' => '57',
            'usiakandungan' => '3',
            'notelpon' => '082330433035',
        ]);
    }
}
